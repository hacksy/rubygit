require 'fileutils'
require "zlib"
require 'digest/sha1'

index = []

def do_update_index(file_list)
  index = read_index()
  file_list.each do |file_sub_dir|
    file_path = File.join Dir.pwd, file_sub_dir
    if !File.exists? file_path
      p "fatal: Unable to process path #{file_sub_dir}"
      return
    end

    # Do something

    if !index.include? file_sub_dir
      index.append(file_sub_dir)
    end
  end
  #Write index
  index_path = File.join Dir.pwd, '.git', 'index'
  output = ""
  # Header
  output += "DIRC"
  output += "\x00\x00\x00\x02" #Version
  output += [index.length].pack("N") #Files Number
  index.each do |file_sub_path|
    #64 bit ctime
    current_file_path = File.join Dir.pwd, file_sub_path
    file_stat = File.stat(current_file_path)
    #64-bit ctime
    output += [file_stat.ctime.to_i].pack("N")
    output += [file_stat.ctime.nsec].pack("N")
    #64-bit mtime
    output += [file_stat.mtime.to_i].pack("N")
    output += [file_stat.mtime.nsec].pack("N")
    #32-bit dev
    # output += [file_stat.dev.to_i].pack("N")
    # Windows
    #output += [file_stat.dev.to_i].pack("N")
    output += "\x00\x00\x00\x00"
    #32-bit ino
    #Lx:output += [file_stat.ino.to_i].pack("N")
    output += "\x00\x00\x00\x00"
    #32-bit mode
    output += [file_stat.mode.to_i].pack("N")
    #32-bit uid
    output += [file_stat.uid.to_i].pack("N")
    # 32-bit gid
    output += [file_stat.gid.to_i].pack("N")
    #32-bit file size
    output += [file_stat.size.to_i].pack("N")
    #160 sha1
    file = File.open(current_file_path, "rb")
    contents = file.read
    file.close
    body = "blob #{contents.length}\0#{contents}"
    sha1 = Digest::SHA1.digest body
    output += sha1
    # 16-bit ‘flags’
    output += "\x00\x01"
    #File name
    output += file_sub_path
    print file_sub_path
    # Null byte
    output += "\x00"
  end
  #hash
  output += Digest::SHA1.digest output
  File.open(index_path, "wb") do |f|
    f.write output
  end
end

def read_index()
  index = []
  index_path = File.join Dir.pwd, ',.git', 'index'
  if !File.exists? index_path
    return index
  end

  return index
end